variable "feeds_json" {
  description = "(Required) A string containing the json description of feeds served by this deployment"
  type        = string
}

variable "republisher_container_image_uri" {
  description = "(Required) the fully qualified uri to the container image"
  type        = string
}

variable "run_interval_minutes" {
  description = "(Optional) The interval in minutes that the republisher lambda function will be executed at"
  type        = number
  default     = 180 # three hours
}

variable "content_expiration_days" {
  description = "(Optional) the number of days after which objects expire from the s3 bucket"
  type        = number
  default     = 30
}

variable "domain_name" {
  description = "(Optional) the domain name the feeds are available at"
  type        = string
  default     = "anynews-republisher"
}

variable "anynews_age_limit_seconds" {
  description = "(Optional) Only items younger than this will be included"
  type        = number
  default     = 30 * 24 * 60 * 60
}
variable "anynews_s3_cache_duration_days" {
  description = "(Optional) cache period - how long should content remain on the retriever site? (in days)"
  type        = number
  default     = 7
}