data "aws_region" "current" {}

resource "aws_s3_bucket" "this" {
  bucket = module.this.id
  acl    = "private"

  versioning {
    enabled = false
  }

  lifecycle_rule {
    id      = "feeds"
    enabled = true
    prefix  = "feeds/"
    expiration {
      days = var.content_expiration_days
    }
  }

  tags = module.this.tags
}

resource "aws_cloudfront_origin_access_identity" "this" {
  comment = "Origin access identity for domain access"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.this.arn}/feeds/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.this.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_s3_bucket_public_access_block" "origin" {
  bucket                  = aws_s3_bucket.this.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  depends_on = [aws_s3_bucket_policy.this]
}

resource "aws_cloudfront_distribution" "this" {
  //  aliases = [var.domain_name]

  origin {
    domain_name = aws_s3_bucket.this.bucket_regional_domain_name
    origin_id   = "S3-${var.domain_name}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-${var.domain_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0 # 3600
    max_ttl                = 0 # 86400
  }

  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_100"
  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  tags = module.this.tags
}

resource "aws_s3_bucket_object" "feeds_json" {
  bucket  = aws_s3_bucket.this.id
  key     = "feeds.json"
  content = var.feeds_json
}

resource "aws_cloudwatch_event_rule" "this" {
  name                = module.this.id
  description         = "Fires every ${var.run_interval_minutes} minutes"
  schedule_expression = "rate(${var.run_interval_minutes} minutes)"
}

resource "aws_cloudwatch_event_target" "this" {
  rule      = aws_cloudwatch_event_rule.this.name
  target_id = "lambda"
  arn       = module.republisher_lambda.lambda_function_arn
}

module "republisher_lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "2.28.0"

  function_name  = module.this.id
  description    = "AnyNews Republisher"
  create_package = false
  image_uri      = var.republisher_container_image_uri
  package_type   = "Image"
  environment_variables = {
    CONSOLE_ECHO                   = "true"
    AWS_S3_BUCKET_NAME             = aws_s3_bucket.this.id
    ANYNEWS_LOCAL_DATA_PATH        = "/tmp/republisher"
    ANYNEWS_AGE_LIMIT_SECONDS      = var.anynews_age_limit_seconds
    ANYNEWS_S3_CACHE_DURATION_DAYS = var.anynews_s3_cache_duration_days
  }
  allowed_triggers = {
    ScheduledTrigger = {
      principal  = "events.amazonaws.com"
      source_arn = aws_cloudwatch_event_rule.this.arn
    }
  }
  attach_policy_statements = true
  policy_statements = {
    accessbucket = {
      effect = "Allow"
      actions = [
        "s3:ListBucket",
        "s3:GetObject",
        "s3:PutObject",
        "s3:DeleteObject"
      ]

      resources = [
        aws_s3_bucket.this.arn,
        "${aws_s3_bucket.this.arn}/*"
      ]
    }
  }
  tags       = module.this.tags
  depends_on = [aws_s3_bucket_object.feeds_json]
}
